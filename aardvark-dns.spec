# trust-dns-{client,server} not available
# using vendored deps

# debuginfo doesn't work yet
%global debug_package %{nil}

%global built_tag_strip 1.14.0

Name: aardvark-dns
Version: %{built_tag_strip}
Packager: Podman Debbuild Maintainers <https://github.com/orgs/containers/teams/podman-debbuild-maintainers>
License: ASL-2.0+
Release: 0%{?dist}
Summary: Authoritative DNS server for A/AAAA container records
URL: https://github.com/containers/%{name}
Source0: %{url}/archive/v%{built_tag_strip}.tar.gz
Source1: %{url}/releases/download/v%{built_tag_strip}/%{name}-v%{built_tag_strip}-vendor.tar.gz
%if "%{_vendor}" == "debbuild"
BuildRequires: cargo
BuildRequires: git
%endif

%description
%{summary}

Forwards other request to configured resolvers.
Read more about configuration in `src/backend/mod.rs`.

%prep
%autosetup -Sgit %{name}-%{built_tag_strip}
tar fx %{SOURCE1}
mkdir -p .cargo

cat >.cargo/config << EOF
[source.crates-io]
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "vendor"
EOF

%build
%{__make} build

%install
%{__make} DESTDIR=%{buildroot} PREFIX=%{_prefix} install

%files
%license LICENSE
%dir %{_libexecdir}/podman
%{_libexecdir}/podman/%{name}

%changelog
